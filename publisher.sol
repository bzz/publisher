pragma solidity ^0.4.9;

contract Publication {
    address public author;
    string public weblink;
    address[] public sources;
    address[] public referrers;
    address[] public positive_reviews;
    address[] public negative_reviews;
    address parentContract;


    function Publication(address _author, string _weblink) public {
        author = _author;
        weblink = _weblink;
        parentContract = msg.sender;
    }

    function setSources(address[] _sources) public {
        sources = _sources;
    }

    function reviewPublicationPositive(address reviewer) public {
        //TODO if not reviewed
        positive_reviews.push(reviewer);
    }
    function reviewPublicationNegative(address reviewer) public {
        //TODO if not reviewed
        negative_reviews.push(reviewer);
    }

    function getAuthor() constant public returns (address) {
        return author;
    }


    function getParentContract() constant public returns (address) {
        return parentContract;
    }

}





contract Publisher {
    event Published(address indexed author, address indexed newAddr, string weblink);

    function Publisher() public {
    }

    address[] public authors;
    mapping(address => string) public contacts;
    mapping(address => address[]) public publications;

    function publish(string weblink) public returns(address) {
        Publication p = new Publication(msg.sender, weblink);
        publications[msg.sender].push(p);
        Published(msg.sender, p, weblink);
        return p;
    }

    function setAuthorContacts(string _contacts) public {
        contacts[msg.sender] = _contacts;
    }


    function setPublicationSources(address publication, address[] sources) public {
        Publication p = Publication(publication);
        p.setSources(sources);
    }

    function testAuthorOf(address publication) constant public returns (address) {
        Publication p = Publication(publication);
        return p.getAuthor();
    }

    function reviewPublicationPositive(address publication) public {
        Publication p = Publication(publication);
        p.reviewPublicationPositive(msg.sender);
    }

    function reviewPublicationNegative(address publication) public {
        Publication p = Publication(publication);
        p.reviewPublicationNegative(msg.sender);
    }


    // function testParent(address publication) constant returns (address) {
    //     Publication p = Publication(publication);
    //     return p.getParentContract();
    // }

}
